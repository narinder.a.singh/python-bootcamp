# -*- coding: utf-8 -*-

from pathlib import Path

# json parsing
# cve
# create file
# absolut and relative path

def csv_to_json():
    """Task01
    Read in the cve file 'userdata.cve' in the data folder
    Create a dict from the data where the first column is the key and the other columns are the value wraped in a list
    Write the dict as json in a new file called 'userdata.json' in the same folder 

    Hint: running the unittests of this assignment will reset the data directory before and after the tests.

    """

    ...

def fun_with_paths():
    """Task02
    Return 
      - the absolute path of this module
      - the relative path from this module to lecture03.pptx
      - the combined path from the previous two
      - all files in this assignemt directory which do not have the .py suffix (do not include the __pycache__ directory!)
    """

    ...