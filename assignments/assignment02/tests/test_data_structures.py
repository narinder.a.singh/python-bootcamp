# -*- coding: utf-8 -*-
import pytest
from copy import deepcopy
import json
from assignment02.data_structures import get_diff_elements_between_list, search_by_ingredient, switch_keys_with_values, append_to_immutable, dict_deepcopy


def test_get_diff_elements_between_list():
    result1 = get_diff_elements_between_list(
        range(1, 10), (2, 4, 6, 8, 10, 10, 12, 14))
    assert isinstance(result1, list)
    assert set(result1) == {1, 3, 5, 7, 9, 10, 12, 14}
    assert result1.count(10) == 1
    result2 = get_diff_elements_between_list(
        ['1', 2, '2', 3, 4, '5', 5], [2, 3, '5'])
    assert isinstance(result2, list)
    assert set(result2) == {'1', '2', 4, 5}


def test_search_by_ingredient():
    input = {
        'apple pie': ['apple', 'flour', 'eggs', 'butter', 'sugar'],
        'fruit salad': ['apple', 'banana', 'grapes'],
        'pudding': ['milk', 'starch', 'chocolate', 'sugar'],
        'pancakes': ['milk', 'flour', 'eggs'],
        'hot choco': ['milk', 'chocolate', 'sugar']
    }
    output = {
        'apple': ['apple pie', 'fruit salad'],
        'milk': ['pancakes', 'pudding', 'hot choco'],
        'flour': ['apple pie', 'pancakes'],
        'eggs': ['apple pie', 'pancakes'],
        'sugar': ['apple pie', 'pudding'],
        'butter': ['apple pie'],
        'banana': ['fruit salad'],
        'grapes': ['fruit salad'],
        'chocolate': ['hot choco', 'pudding']
    }

    r = search_by_ingredient(input)
    for k in output.keys():
        assert k in r
        assert r[k].sort() == output[k].sort()


def helper1(x):
    if isinstance(x, list):
        return tuple(x)
    elif isinstance(x, set):
        return frozenset(x)
    else:
        return x


def helper2(d):
    d2 = deepcopy(d)
    for _ in range(len(d2)):
        k, v = d2.popitem()
        yield k, v


def test_switch_keys_with_values():
    d = {
        'a': 1,
        'b': 2,
        'c': [42]

    }
    r = switch_keys_with_values(d)

    for dk, dv in helper2(d):
        try:
            r.pop(helper1(dv))
            print(f"{dk} and {dv} were successfully switched")
        except KeyError:
            print(
                f"{helper1(dv)} was not found as key in switch_keys_with_values result")
            assert False


def test_append_to_immutable():
    r1 = append_to_immutable((97, 98, 99), 100)
    r2 = append_to_immutable(frozenset({'apple', 'pear'}), 'grape')
    assert r1 == (97, 98, 99, 100)
    assert r2 == frozenset({'apple', 'pear', 'grape'})


def test_dict_deepcopy():
    test_dict = {
        "a": 2,
        "b": lambda x: x*x,
        "e": {
            "C": 2,
            "F": 1
        },
        "d": ['Foo', 'Bar', 'Baz', {
            'fruits': ['apple', 'banana', 'cherry']
        }]
    }
    result = dict_deepcopy(test_dict)
    # easy but slow way to compare dicts recursively
    assert json.dumps(result, sort_keys=True, indent=None) == json.dumps(
        test_dict, sort_keys=True, indent=None)
    test_dict['e']['F'] = "The answer to everything"
    assert result['e']['F'] == 2
    test_dict['d'][1] = 42
    assert result['d'][1] == 'Bar'
    test_dict['d'][3]['fruits'].append('durian')
    assert len(result['d'][3]['fruits']
               ) == 3 and 'durian' not in result['d'][3]['fruits']
    assert result['b'](4) == 16
